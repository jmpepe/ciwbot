import discord
from discord.ext import commands
import asyncio
import logging
import sys
import os.path
import io
import copy
import re
import random
import collections

# Server-channel response blacklist
BLACKLIST_PATH = "res/blacklist.dat"
blacklist = []
if os.path.isfile(BLACKLIST_PATH):
	print("Reading blacklist.dat...")
	blacklistFile = open(BLACKLIST_PATH, 'r')
	line = blacklistFile.readline()
	while line:
		servchan = line + blacklistFile.readline().rstrip('\n')
		# With this format, the file will be:
		# 0	Server Name
		# 1	Channel name
		# 2 Server Name
		# etc.
		# From this, the blacklist List will be items of servername+channelname,
		# delimited by a newline between the two.
		blacklist.append(servchan)
		line = blacklistFile.readline()
		print('+' + servchan)
	blacklistFile.close()
	
# Slot persistence
SLOTS_PATH = "res/slots.dat"
slotstats = {}
if os.path.isfile(SLOTS_PATH):
	print("Reading slots.dat...")
	slotsFile = open(SLOTS_PATH, 'r')
	line = slotsFile.readline()
	while line:
		ex = re.compile(":")
		slotstats[ex.split(line)[0]] = ex.split(line)[1]
		print('+' + line)
		line = slotsFile.readline()
	slotsFile.close()

# Quotes
# [number]`"Quote quote quote quote"`-author
QUOTES_PATH = "res/quotes.dat"
quotes = {}
if os.path.isfile(QUOTES_PATH):
	print("Reading quotes.dat...")
	quotesFile = open(QUOTES_PATH, 'r')
	line = quotesFile.readline()
	while line:
		ex = re.compile("`")
		quotes[ex.split(line)[0]] = [ex.split(line)[1], ex.split(line)[2].rstrip('\n')]
		line = quotesFile.readline()
	quotesFile.close()

# Discord!
ciwbot = commands.Bot(command_prefix="!", description="help me")
speakChannel = None

# Debug logging
logger = logging.getLogger("discord");
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename="discord.log", encoding="utf-8", mode='w')
handler.setFormatter(logging.Formatter("%(asctime)s:%(levelname)s:%(name)s: %(message)s"))
logger.addHandler(handler)

@ciwbot.event
async def on_ready():
	print(ciwbot.user.name + " (" + ciwbot.user.id + ") ready to heck it up yall")
	print("----")

@ciwbot.event
async def on_message(m):
	if m.author.id != "184806249773137921": #ciwbot's id
		chan = m.channel
		if chan.is_private:
			print("Private message from " + m.author.name)
			if m.author.id == "100269355379466240": #my id
				# Allow me to log ciwbout out from PMs with "logout"
				if m.content == "logout" or m.content == 'c':
					await logout(m.author)
				# speakAs commands
				await setSpeakChannel(m.content, m.author)
				await speakAs(m.content, m.author)
			else: #other people
				await ciwbot.send_message(m.author, "Hi! I don't parse messages that aren't from JM. JM also chose not to read what you sent me, which means that I too cannot read it. Also, I have a very bad memory, so forgive me if you've heard this before.")
		else:
			print("Message on channel \"" + chan.name + "\", server \"" + chan.server.name + "\"")
			await postHelp(m)
			if not onBlacklist(chan):
				await addBlacklist(m)
				await checkGreeting(m)
				await sharePorn(m)
				await sharePizza(m)
				await playSlots(m)
				await addQuote(m)
				await getQuote(m)
				await delQuote(m)
			else:
				await removeBlacklist(chan, m.content)
				
# Display a help message
# > msg: the message object
# > auth: the message author
@asyncio.coroutine
async def postHelp(msg):
	ex = re.compile("^!(help)|(commands)")
	if ex.search(msg.content):
		print("help: " + msg.channel.name)
		await ciwbot.send_message(msg.channel, "Here is a list of commands I'll respond to:\n" +
			"!blacklist - prevent me from interacting with this channel\n" +
			"!whitelist - allow me to interact with this channel\n" +
			"!quote - show a random quote\n" +
			"!quote [number] - show quote #[number]\n" +
			"!addquote \"Content of quote\" -Author of quote - add a quote\n" +
			"!delquote [number] - delete quote #[number]\n" +
			"\n" +
			"JM is excellent at making spaghetti. Don't believe me? Look at my source code:\n" +
			"https://bitbucket.org/jmpepe/ciwbot/src/")

# Check if a message contains ciwbot's name
# > msg: the message content
# < boolean: True if ciwbot's name was matched
def mentionedName(msg):
	ex = re.compile("ciwb{1,2}ot", re.IGNORECASE)
	return ex.search(msg.content) or (ciwbot.user in msg.mentions)

# Check if message is a greeting to ciwbot
# > msg: the message object
@asyncio.coroutine
async def checkGreeting(msg):
	ex = re.compile("(^|\s+)(h(i|(e(y|l{2}o))))|(yo)(\s|$)|(what'?s\s+(up)|(go{1,2}d))", re.IGNORECASE)
	wrongex = re.compile("ciwb{2}ot", re.IGNORECASE)
	greet = ex.search(msg.content)
	heardWrongName = wrongex.search(msg.content)
	if greet and mentionedName(msg):
		await ciwbot.send_message(msg.channel, "Hi " + msg.author.name + "!")
		if heardWrongName:
			await ciwbot.send_message(msg.channel, "By the way, it's just 1 B.")

# Checks whether or not to add a channel to the channel interaction blacklist via message
# > msg: the message object
@asyncio.coroutine
async def addBlacklist(msg):
	# keywords: "!blacklist", message must start with '!'
	ex = re.compile("^!blacklist")
	if ex.search(msg.content):
		item = msg.server.name + '\n' + msg.channel.name
		blacklist.append(item)
		print("Appended channel:\n" + item)
		await ciwbot.send_message(msg.channel, "I'll no longer respond on \"" + msg.channel.name + "\". To add me back, type \"!whitelist\".")
		
# Checks whether or not to remove the channel from the interaction blacklist via message
# > chan: the channel
# > msg: the message content
@asyncio.coroutine
async def removeBlacklist(chan, msg):
	# keywords: "!whitelist", message must start with '!'
	if "!whitelist" in msg and msg[0] == '!':
		servchan = chan.server.name + '\n' + chan.name
		try:
			blacklist.remove(servchan)
		except ValueError:
			pass
		print("Blacklist updated.")
		await ciwbot.send_message(chan, "Hi! I'll be active in this channel now.")

# Check if ciwbot is blacklisted on this server/channel
# > chan: the channel
# < boolean: True if ciwbot is blacklisted
def onBlacklist(chan):
	for servchan in blacklist:
		if servchan == chan.server.name + '\n' + chan.name:
			return True
	
	return False

# Direct ciwbot to speakAs in this channel
# !setspeak Server Name, Channel Name
# > msg: the message content
# > auth: the author of the message
@asyncio.coroutine
async def setSpeakChannel(msg, auth):
	global speakChannel
	ex = re.compile("^!setspeak\s+")
	ex2 = re.compile(",\s+")
	if ex.search(msg):
		try:
			msg = ex.split(msg)[1]
			serv = ex2.split(msg)[0]
			chan = ex2.split(msg)[1]
			
			# Find channel through list
			for botServ in ciwbot.servers:
				for botChan in botServ.channels:
					if botServ.name == serv and botChan.name == chan:
						await ciwbot.send_message(auth, "Messages will be sent to " + chan + " in " + serv + ".")
						speakChannel = copy.copy(botChan)
						print("!setspeak successful:")
						print(speakChannel.name + " in " + speakChannel.server.name)
		except IndexError:
			print("!setspeak location not found or invalid")
			await ciwbot.send_message(auth, "Channel given invalid. Format:")
			await ciwbot.send_message(auth, "!setspeak Server Name, Channel Name")
			pass
	
# Speak as ciwbot through PM
# !speak this is the message
# > msg: the message content
# > auth: the author of the message
@asyncio.coroutine
async def speakAs(msg, auth):
	ex = re.compile("^!speak\s+")
	if ex.search(msg):
		if not speakChannel:
			await ciwbot.send_message(auth, "Please set a channel to speak in first. Format:")
			await ciwbot.send_message(auth, "!setspeak Server Name, Channel Name")
		else:
			post = ex.split(msg)[1]
			await ciwbot.send_message(auth, "Posting to " + speakChannel.name + " in " + speakChannel.server.name + "...")
			await ciwbot.send_message(speakChannel, post)
			print("!speak successful")
			
# Nope.
@asyncio.coroutine
async def sharePorn(msg):
	ex = re.compile("^!porn")
	if ex.search(msg.content) and msg.channel.name == "nsfw":
		await ciwbot.send_message(msg.channel, "Probably not.")
		
# Share some pizza
# > msg: the message object
@asyncio.coroutine
async def sharePizza(msg):
	ex = re.compile("^!pizza")
	if ex.search(msg.content):
		nsfwChan = discord.utils.get(msg.server.channels, name="nsfw")
		await ciwbot.send_message(nsfwChan, "Sorry, I don't have any pizza right now. Working on that.")
		
# Play slots
# > msg: the message object
@asyncio.coroutine
async def playSlots(msg):
	ex = re.compile("^!slots")
	if ex.search(msg.content):
		vegasChan = discord.utils.get(msg.server.channels, name="vegas")
		await ciwbot.send_message(vegasChan, "Slots are under maintenance right now, sorry.")
		print(msg.author.name + " is playing slots!")
	
# Add a quote to a quote bot list
# Format: !addquote "This is a quote" -someone's name
# Quotes are stored in a dictionary with strings of number as keys. Adding a quote
# creates a new entry in the dictionary with a key of a number one higher than the
# current highest number in the dictionary key list.
# > msg: the message object
@asyncio.coroutine
async def addQuote(msg):
	ex = re.compile("^!addquote")
	if ex.search(msg.content):
		highestKey = sorted(quotes.keys())[-1]
		newKey = str(int(highestKey) + 1)
		quotex = re.compile("\".*\"")
		authex = re.compile("-\S+.*")
		theQuote = (quotex.search(msg.content)).group(0)
		theAuthor = (authex.search(msg.content)).group(0)
		quotes[newKey] = [theQuote, theAuthor]
		print("Added quote #" + newKey)
		await ciwbot.send_message(msg.channel, "Quote added (#" + newKey + ").")

# Retrieve a quote from the quote bot list
# Format: !quote [number]
# Quotes are stored in a dictionary with strings of numbers as keys. 
# > msg: the message object
@asyncio.coroutine
async def getQuote(msg):
	purex = re.compile("^!quote\s*$")
	numex = re.compile("^!quote\s+#?\d+$")
	phrase = numex.search(msg.content)
	if purex.search(msg.content):
		# Read a random quote
		if len(quotes.keys()) == 0:
			await ciwbot.send_message(msg.channel, "There are no quotes yet!")
		else:
			randKey = random.choice(sorted(quotes.keys()))
			try:
				theQuote = quotes[randKey]
				await ciwbot.send_message(msg.channel, theQuote[0] + " " + theQuote[1] + " (#" + randKey + ")")
			except KeyError:
				print("Randquote retrieved a number out of bounds!")
				await ciwbot.send_message(msg.channel, "Sorry, something went wrong. I'll go tell JM.")
				me = discord.utils.get(msg.server.members, id="100269355379466240")
				await ciwbot.send_message(me, "Randquote retrieved a number out of bounds in " + msg.server.name + " - " + msg.channel.name)
				pass
	elif phrase:
		# Read a quote by number
		spacex = re.compile("\s+")
		hashex = re.compile("\d+")
		number = spacex.split(phrase.group(0))[1] # Just the number part
		pureNumStr = hashex.search(number).group(0) # Without the potential '#'
		
		# Read quote
		print("Reading quote " + pureNumStr)
		try:
			theQuote = quotes[pureNumStr]
			await ciwbot.send_message(msg.channel, theQuote[0] + " " + theQuote[1] + " (#" + pureNumStr + ")")
		except KeyError:
			print("Quote does not exist")
			await ciwbot.send_message(msg.channel, "Quote #" + pureNumStr + " does not exist!")
			pass
			
# Removes a quote by number from the quote list
# > msg: the message object
@asyncio.coroutine
async def delQuote(msg):
	numex = re.compile("^!delquote\s+#?\d+$")
	phrase = numex.search(msg.content)
	if phrase:
		spacex = re.compile("\s+")
		hashex = re.compile("\d+")
		number = spacex.split(phrase.group(0))[1] # Just the number part
		pureNumStr = hashex.search(number).group(0) # Without the potential '#'
		
		# Delete quote
		print("Deleting quote " + pureNumStr)
		try:
			theQuote = quotes[pureNumStr]
			del quotes[pureNumStr]
			await ciwbot.send_message(msg.channel, "Quote #" + pureNumStr + " deleted.")
		except KeyError:
			print("Cannot delete nonexistent quote #" + pureNumStr)
			await ciwbot.send_message(msg.channel, "Quote #" + pureNumStr + " does not exist!")
			pass

# Log ciwbot out
@asyncio.coroutine
async def logout(auth):
	# Save blacklist
	print("Saving blacklist...")
	f = open(BLACKLIST_PATH, 'w')
	for item in blacklist:
		print("+" + item)
		f.write(item + '\n')
	f.close()
	print("Saved.")
	
	# Save quotes
	print("Saving quotes...")
	f2 = open(QUOTES_PATH, 'w')
	for key in sorted(quotes.keys()):
		print("+" + quotes[key][0] + " " + quotes[key][1])
		f2.write(key + "`" + quotes[key][0] + "`" + quotes[key][1] + '\n')
	f2.close()
	print("Saved.")
	
	# Finally, log out
	await ciwbot.send_message(auth, "Logging out...")
	print("Logging out via request...")
	await ciwbot.logout()
	
# Error printing help
# > m: message string
def printerr(m):
	print(m, file=sys.stderr)
	
if os.path.isfile("res/back/TOKEN"):
	tokenFile = open("res/back/TOKEN")
	token = tokenFile.readline()
	tokenFile.close()
	ciwbot.run(token)
else:
	printerr("TOKEN not found!")
	printerr("Exiting...")
	sys.exit(1)