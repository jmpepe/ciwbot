# ABOUT CIWBOT #

### Purpose ###

"ciwbot" is a Discord bot meant for my own learning. It has basic features such as greeting responses, and a quotes system. It is programmed in Python 3 using the discord.py API.

### How do I use ciwbot? ###

ciwbot isn't meant to be used outside of whatever functions cater to my server mates' and my interest, so its use is limited. That said, if you would like to use ciwbot for your server, keep in mind that it is a constant work in progress and probably will not include features beyond my curiosity. Now that you have been warned... [add ciwbot to your server](https://discordapp.com/oauth2/authorize?&client_id=184806010395820032&scope=bot&permissions=125982)!

### Source ###

Requirements:  
 - A Discord developer application/bot [https://discordapp.com/developers/applications/me]  
 - Python 3.5.1  
 - discord.py [https://github.com/Rapptz/discord.py]

The Discord developer bot will give you a bot token: this token needs to be on a single line in a file located at [ciwbot location]/res/back/TOKEN (note that TOKEN has no extension). The bot will also create files for persistence in [ciwbot location]/res/.